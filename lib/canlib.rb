require 'socket'
require 'ostruct'

require_relative 'r/can.rb'

class Canlib < CANsock 

    def info
        puts "A SocketCAN user-space interface to work with CAN protocol."
        puts "Brought to you with love."
        puts "Halis Duraki <duraki.halis@nsoft.ba>"
    end

end

