require 'socket'
require 'io/console'

require_relative 'helpers.rb'
require_relative 'packet.rb'

class CANsock < Socket

    PF_CAN  = 29
    AF_CAN  = PF_CAN
    CAN_RAW = 1
    SIOCGIFINDEX = 0x8933

    def initialize()
        p "CAN init"
    end

    def int_connect(interface)
        @socket = Socket.new(PF_CAN, Socket::SOCK_RAW, CAN_RAW)

        if_idx_req = interface.ljust(16, "\0")+[0].pack("L")
        @socket.ioctl(SIOCGIFINDEX, if_idx_req)

        if_name, if_index = if_idx_req.unpack("A16L")

        sockaddr_can = [AF_CAN, if_index, 0, 0, 0].pack("SlLLS")
        @socket.bind(sockaddr_can)

        int_read
    end

    def int_send(data)
       #@socket.send "DEADBEEF", 0 
    end

    def int_read
        while true do
            canrecv =   CanPacket.read(@socket.read(16))
            canrecv.add_frame(canrecv)
        end
    end

end
