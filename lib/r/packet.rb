require 'bindata'
require 'ostruct'

class CanPacket < BinData::Record
    endian :little

    uint32 :can_id
    uint8  :can_dlc
    uint8  :pad
    uint8  :res0
    uint8  :res1
    array  :data, :type => :uint8, :initial_length => 8

    # Define frame structure
    @@frame = OpenStruct.new
    @@frame.index	  # Index of the frame data (ofs)
    @@frame.can_id        # Frame CAN id
    @@frame.can_dlc       # Frame CAN dlc
    @@frame.pad           # Padding__
    @@frame.data          # Frame data
    @@frame.chars         # Frame data (chr)
    @@frame.hex           # Frame data (hex)
    @@frame.text          # Frame data (text)
    @@frame.initlen       # Initial lenght
    @@frame.len           # Frame lenght
    @@frame.fbyte         # Frame first byte

    @@packets = []         # Hold all recorded frame data

    # add_frame 
    # Add frame data to the @@packets
    def add_frame bindata
        p "Bindata frame in oStruct"

	# Invoke total
	return_packet_stack

        p @@frame.inspect
	@@frame.index	= @@total + 1
        @@frame.can_id  = bindata.can_id
        @@frame.can_dlc = bindata.can_dlc
        @@frame.pad     = bindata.pad
        @@frame.initlen = 8 
        @@frame.data    = bindata.data.to_ary
        @@frame.data    = remove_frame_pad @@frame.data
        @@frame.chars   = @@frame.data
        @@frame.fbyte   = @@frame.data.first
        @@frame.hex     = @@frame.data.each.map {|x| x.ord.to_s(16)}

	# Push to packets_arr
        push_frame_to_data @@frame

    end

    # Return no of frames in packets stack
    def return_packet_stack
	@@total = @@packets.count
    end

    # Return the frame data by packet index
    def return_frame_by_index index
	p @@packets[index].inspect
    end

    # Remove padding from the frame data (pre-bindata)
    def remove_frame_pad frame
        frame.delete_if {|x| x.to_s.length == 1}
    end 

    # Push frame to @@packets arr
    def push_frame_to_data frame
        @@packets << @@frame
    end

    # Clean @@packets memory
    def clean_frame_data_mem packets
        @@packets.clear
    end
end
