def htonl(h)
    [h].pack("L").unpack("N")[0]
end

def htons(h)
    [h].pack("S").unpack("n")[0]
end

def to_bits(val)
	return "%0#{val.size * 4}d" % (val.hex.to_s(2))
end

def identifier(val)
	return val[0..2]
end

def dlc(val)
  val[3].hex
end

def data_field(val)
  val[4..-5]
end

def crc(val)
  val[-4..-1]
end

def format_data(identifier, dlc, data_field_bytes)
  [identifier, dlc, data_field_bytes.join(' '), crc].join ' '
end

def match(val, integer)
  # data_field_values.select { |value| value.hex == integer }.size > 0
end

def find_by_identifier(frames, identifier)
  frames.select { |f| f.identifier == identifier }
end

def find_by_value(frames, value)
  frames.select { |f| f.match(value) }
end

def formatted_output(frames)
  frames.map { |f| f.formatted_output }.join("\n")
end

def match_identifier_with_value(frame, id, value)
  find_by_value(frame, value) & find_by_identifier(frame, id)
end

def write_frames(file, frames)
  File.open file, 'w' do |f|
    f.write formatted_output(frames)
  end  
end
