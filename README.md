### canlib.rb
**canlib**.rb is a Ruby (currently) library (not a gem yet) that allow developers to communicate and intercept the CAN traffic. CAN bus is vehicle bus standard, so with this library you are able to actually command the car to do some action. Cool, right? An origin of this project comes from another product of mine called **canhell** which represents one of a main functionallity of the library. **canhell** is a CAN analyzer and forensic tool built to sniff and inject various CAN data and frames on vehicle system.

### requirements
**canlib.rb** is powered by Ruby. A beautiful language with not such beautiful library code (but that will change with refactorings). Bellow are requirements for the library.

#### \*nix based vs Windows
I'll go easy with this one. The library is developed on Linux with CAN protocol so kernel `2.6.25` & up. The modules and drivers I'm talking about are [SocketCAN](https://en.wikipedia.org/wiki/SocketCAN) which is supported in BSD. So you might tought about implementation of the same drivers is possible in OS X? Well, it is. But it's pain in the a++. As an example, I'll leave my implementation for OSX that is rewritten from Linux/BSD drivers to the native C compiled library. Currently only sending/injecting the data is possible, but I promise I'll update the library when I get time.  
  
Windows is quite interesting. As always, you find the possible DLL and you try to make it work with that particurale DLL. Finding the documentation is always a fun process and you get a middle finger by Microsoft for trying to do something like that. So I always skip it :-)

To short it up:  
* Linux Kernel `2.6.25` and up is fully supported, natively!
* OSX - Not supported. **socketcanx** is slowly being developed since I'm focused on other things.
* Windows - Possible, but I won't help you with this one.

#### Ruby
* install rvm because why not?
* do something like `rvm install 2.0.0` 
* pay attention to the version I'm telling you
* it's **2.0.0**
* set it as a default or use rvms command to run it from particular version

#### Ruby dependencies
* bindata `gem install bindata`

#### Running
* Enter the directory `cd canlib.rb` 
* Execute the tests `ruby tests/testname.rb`
* Passed? Great!

#### Library documentation
* Check the [Documentation.md](docs/Documentation.md)

### Resources
	*  #! https://github.com/st3fan/osx-10.9/blob/master/libpcap-42/libpcap/pcap-can-linux.c
	*  # https://docs.python.org/3/library/socket.html#socket.PF_CAN
	*  # http://src.gnu-darwin.org/src/sys/pc98/pc98/canbus.c.html
	*  # https://www.kernel.org/doc/Documentation/networking/can.txt
