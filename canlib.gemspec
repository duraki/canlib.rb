Gem::Specification.new do |s|
  s.name        = 'canlib'
  s.version     = '0.0.0'
  s.date        = '2016-09-17'
  s.summary     = "A SocketCAN uint for Ruby."
  s.description = "A module integration for your Ruby art to work with CAN protocol."
  s.authors     = ["Halis Duraki"]
  s.email       = 'duraki.halis@nsoft.com'
  s.files       = ["lib/canlib.rb"]
  s.homepage    =
    'http://github.com/dn5/canlib.rb'
  s.license       = 'MIT'
end